from http.server import HTTPServer, BaseHTTPRequestHandler #Import http server

HOST = '0.0.0.0'
PORT = 8080

class Handler(BaseHTTPRequestHandler):                 #Handler class based in http request handler 
     def do_GET(self):                                 #And define a get method
        self.send_response(200)                        #Setting the response status to 200
        self.send_header('Content-Type', 'text/plain') #Set the content type header to text type
        self.end_headers()

        message = 'Hello World!'
        self.wfile.write(bytes(message, 'utf-8'))      #Set encoding type to utf-8

def run(host, port, server_class=HTTPServer, handler_class=Handler): #Define another run function to initialize the server
    server_address = (host, port)
    httpd = server_class(server_address, handler_class)

    try:
        httpd.serve_forever()
    except KeyboardInterrupt:
        pass

    httpd.server_close()

if __name__ == '__main__': #Pass the host and the port to the run function
    run(HOST, PORT)
                            #Create Virtual environments to keep app packages isolated from my environment