#The goal is to create a small image as possible that includes fewer packages. 
#This is a standard image form dockerhub
#Copy main.py to the image 
#And define entrypoint for the image to start the server

FROM python:3.11.2 

COPY main.py .

ENTRYPOINT [ "python3", "main.py"]